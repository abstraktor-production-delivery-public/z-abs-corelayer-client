
'use strict';

import { DataActionDialogFileGet } from '../actions/action-dialog-file';
import StoreBaseData from '../store/store-base-data';
import Project from 'z-abs-corelayer-cs/clientServer/project';


class DialogFileStore extends StoreBaseData {
  constructor() {
    super({
      project: new Project(),
      current: {
        file: null,
        folder: null,
        type: ''
      },
      original: {
        file: null,
        folder: null,
        type: ''
      },
      workspace: '',
      workspaces: []
    });
  }
  
  onActionDialogFileSet(action) {
    let node = this.state.project.findNode(action.path);
    if(node) {
      if(node.data.path.startsWith(`${this.state.original.folder.data.path}/${this.state.original.folder.title}`)) {
        this.updateState({current: {type: {$set: node.folder ? 'folder' : 'file'}}});
        if(node.folder) {
          this.updateState({current: {folder: {$set: node}}});
        }
        else {
          this.updateState({current: {file: {$set: node}}});
        }
      }
    }
  }
  
  onActionDialogWorkspaceSet(action) {
    this.updateState({workspace: {$set: action.workspace}});
  }
  
  onActionDialogFileGet(action) {
    this.updateState({project: {$set: new Project()}});
    this.updateState({current: {$set: {
      file: null,
      folder: null,
      type: ''
    }}});
    this.updateState({original: {$set: {
      file: null,
      folder: null,
      type: ''
    }}});
    this.updateState({workspace: {$set: ''}});
    this.updateState({workspaces: {$set: []}});
    this.sendDataAction(new DataActionDialogFileGet(action.path, action.rootName, action.pathFilters, action.existingFilters, action.type, action.projectType, action.plugin, action.workspaceName));
  }
  
  onDataActionDialogFileGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if('workspace' === action.type) {
        this.updateState({workspaces: {$set: response.data}});
      }
      else {
        this.updateState({project: (project) => { project.set(response.data.source, response.data.type); }});
        const node = this.state.project.findNode(action.path);
        if(node) {
          this.updateState({current: {file: {$set: null}}});
          this.updateState({current: {folder: {$set: node}}});
          this.updateState({current: {type: {$set: 'folder'}}});
          this.updateState({original: {file: {$set: null}}});
          this.updateState({original: {folder: {$set: node}}});
          this.updateState({original: {type: {$set: 'folder'}}});
        }
      }
    }
  }
}

module.exports = new DialogFileStore();
