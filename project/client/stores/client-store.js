
'use strict';

import { DataActionClientGet } from '../actions/action-client';
import StoreBaseData from '../store/store-base-data';


class ClientStore extends StoreBaseData {
  constructor() {
    super({
      wsWebServers: [],
      wsWebClients: []
    });
  }
  
  onDataActionClientGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      console.log(response.data);
    }
  }
}


module.exports = ClientStore;
