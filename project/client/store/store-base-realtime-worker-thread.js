
'use strict';

const WorkerThread = require('../worker/worker-thread');


class StoreBaseRealtimeThread extends WorkerThread{
  constructor() {
    super();
  }
    
  onInit() {
    
  }
  
  onExit() {
    
  }
  
  onMessage() {
    
  }
}


module.exports = StoreBaseRealtimeThread;
