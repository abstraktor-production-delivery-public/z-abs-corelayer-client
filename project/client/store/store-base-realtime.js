
'use strict';

import WorkerMainCoreProtocol from '../worker/worker-main-core-protocol';
import WorkerThreadCoreProtocol from '../worker/worker-thread-core-protocol';
import StoreBaseData from './store-base-data';
import Action from '../communication/action';
import SubscriptionAction from '../communication/subscription-action';
import DataConfig from '../communication/data-config';
import MessageRealtimeError from '../communication/messages/messages-client/message-realtime-error';
import MessageRealtimeOpen from '../communication/messages/messages-client/message-realtime-open';
import MessageRealtimeClosed from '../communication/messages/messages-client/message-realtime-closed';
import MessagePersistentInitRequest from '../communication/messages/messages-c-to-s/message-persistent-init-request';
const Decoder = requireAsBundle('../communication/core-protocol/decoder');
import AppDecoder from '../communication/core-protocol/decoder';
import CoreProtocolConst from 'z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const';
import DeserializerMessageResponse from 'z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer-message-response';
const Deserializer = requireAsBundle('z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer');
const CoreMessage = requireAsBundle('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-message');
import AppDeserializer from 'z-abs-corelayer-cs/clientServer/communication/app-protocol/app-deserializer';
import TextCache from 'z-abs-corelayer-cs/clientServer/communication/cache/text-cache';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';


export default class StoreBaseRealtime extends StoreBaseData {
  static TO_WORKER_OPEN = 0;
  static TO_WORKER_MESSAGE = 1;
  static TO_WORKER_REQUEST = 2;
  static TO_WORKER_CLOSE = 3;
  
  static FROM_WORKER_OPENED = 0;
  static FROM_WORKER_MESSAGE = 1;
  static FROM_WORKER_RESPONSE = 2;
  static FROM_WORKER_ERROR = 3;
  static FROM_WORKER_CLOSED = 4;
  
  static textCache = new TextCache('StoreBaseRealtime');
  
  static plugins = [];
  static {
    const scripts = window.document.getElementsByTagName("script");
    const start = '/abs-scripts/PluginProtocolLayer-';
    for(let script of scripts) {
      const index = script.src.indexOf(start);
      if(-1 !== index) {
        const prefix = 'plugin_protocol_client';
        const protocolName = 'z-plugin-protocol-' + script.src.substring(index + start.length, script.src.length - 3).toLowerCase();
        const name = protocolName.substring(2 + prefix.length, protocolName.length - 3).replaceAll('-', '_');
        try {
          const Plugin = require(`${protocolName}/clientServer/plugin-protocol/plugin_protocol${name}`);
          StoreBaseRealtime.plugins.push(new Plugin());
        } catch(e) {
          ddb.error(e);
        }
      }
    }
  }
  
  constructor(state, subscribeServices) {
    super(state);
    this.subscription = false;
    this.subscribeServices = subscribeServices ? subscribeServices : [];
    this.realtime = true;
    this.realtimeId = 0;
    this.realtimeListerners = new Map();
    this.workerCoreProtocol = null;
    this.requests = new Map();
    if(0 !== this.subscribeServices.length) {
      this._initWebSocket();
    }
    this.appDeserializer = new AppDeserializer(new AppDecoder(StoreBaseRealtime.textCache), new AppDecoder());
    this.initialized = false;
  }

  _isActive() {
    return 0 !== this.realtimeListerners.size || 0 !== this.subscribeServices.length;
  }
  
  sendRealtimeAction(realtimeAction, sessionId) {
    if(this._isActive()) {
      this._sendRequest(realtimeAction, sessionId);
    }
  }
  
  sendRealtimeMessage(msg, dataBuffers) {
    if(this._isActive()) {
      this.workerCoreProtocol.sendRealtimeMessage(msg, dataBuffers);
    }
  }
  
  handleRealtimeMessage(msg) {
    this.realtimeListerners.forEach((listener) => {
      listener.cbMessage(msg);
    });
    this.dispatching.on = true;
    this.dispatchRealtimeMessageAction(msg);
    this.dispatching.on = false;
    this._updateState();
  }
  
  addRealtimeListener(name, cbMessage) {
    if(0 === this.realtimeListerners.size) {
      this._initWebSocket();
    }
    this.realtimeListerners.set(++this.realtimeId, {
      name: name,
      cbMessage: cbMessage
    });
    const realtimeId = this.realtimeId;
    return {
      remove: () => {
        this.removeRealtimeListener(realtimeId);
      }
    }
  }
  
  removeRealtimeListener(realtimeId) {
    this.realtimeListerners.delete(realtimeId);
    if(0 === this.realtimeListerners.size) {
      this.workerCoreProtocol.wsDisconnect();
    }
  }
  
  onStarted(...params) {
    DataConfig.login(() => {
      const wsServer = DataConfig.wsServers.get('ws-web-server');
      this.workerCoreProtocol.wsConnect(`ws://${wsServer.host}:${wsServer.port}`);
    });
  }
  
  onConnected() {
    this.workerCoreProtocol.init();
    this.handleRealtimeMessage(new MessageRealtimeOpen());
  }
  
  onInitialized(...params) {
    if(!this.initialized) {
      this.initialized = true;
      this.appDeserializer.register(CoreProtocolConst.RESPONSE, null, new DeserializerMessageResponse());
      StoreBaseRealtime.plugins.forEach((plugin) => {
        plugin.registerDeserializer(this.appDeserializer);
      });
    }
    if(0 !== this.subscribeServices.length) {
      this.sendRealtimeMessage(new MessagePersistentInitRequest(this.constructor.name, this.subscribeServices));
    }
  }
  
  onDisconnected() {
    this.workerCoreProtocol.stop();
    this.workerCoreProtocol = null;
    this.handleRealtimeMessage(new MessageRealtimeClosed());
    if(this._isActive()) {
      this._initWebSocket();
    }
  }
  
  onStopped(...params) {}
  
  onError() {
    this.handleRealtimeMessage(new MessageRealtimeError());
  }
  
  onCoreProtocolMessage(coreMessage) {
    if(coreMessage.isBinary) {
      if(coreMessage.hasTextCache) {
        this.appDeserializer.doCachedText(coreMessage.cachedTextBuffer);
      }
      if(0 !== coreMessage.msgId && this.appDeserializer) {
        const msg = this.appDeserializer.do(coreMessage.msgId, coreMessage.bin, false);
        if(!msg) {
          return;
        }
        if(CoreProtocolConst.RESPONSE === coreMessage.msgId) {
          const cb = this.requests.get(msg.id);
          if(cb) {
            this.requests.delete(msg.id);
            cb(msg);
          }
        }
        else if(CoreProtocolConst.PERSISTENT_PUBLISH === coreMessage.msgId) {
          this.handleRealtimeMessage(msg);
        }
        else if(CoreProtocolConst.PERSISTENT_INIT_RESPONSE === coreMessage.msgId) {
          this.subscription = true;
        }
        else {
          this.handleRealtimeMessage(msg);
        }
      }
    }
    else {
      this.appDeserializer.doMsg(coreMessage.msg);
      if(CoreProtocolConst.PERSISTENT_PUBLISH === coreMessage.msgId) {
        this.dispatching.on = true;
        const handler = Reflect.get(this, `onSubscribe${coreMessage.msg.data.name}`);
        if(typeof handler === 'function') {
          const subscriptionAction = new SubscriptionAction(coreMessage.msg.data);
          if(undefined !== Reflect.apply(handler, this, [subscriptionAction])) {
            ddb.warning(`${this.constructor.name} store handler must not return values.`);
          }
        }
        else {
          ddb.warning(`REALTIME_PUBLISH: 'onSubscribe${coreMessage.msg.data.name}, ' is not handled in the Store: '${coreMessage.msg.storeName}'`);
        }
        this.dispatching.on = false;
        this._updateState();
      }
      else if(CoreProtocolConst.PERSISTENT_INIT_RESPONSE === coreMessage.msg.msgId) {
        this.subscription = true;
        return;
      }
      else {
        this.handleRealtimeMessage(coreMessage.msg);
      }
    }
  }
  
  _initWebSocket() {
    this.workerCoreProtocol = new WorkerMainCoreProtocol(this, 'WorkerThreadCoreProtocol', Decoder, Deserializer, CoreMessage);
    this.workerCoreProtocol.start();
  }
  
  dispatchRealtimeAction(realtimeAction) {
    realtimeAction.actions.forEach((action) => {
      this.dispatching.action = action;
      this.dispatching.on = true;
      const handler = Reflect.get(this, `onRealtime${action.constructor.name}`);
      if(typeof handler === 'function') {
        if(undefined !== Reflect.apply(handler, this, [action])) {
          ddb.warning(`${this.constructor.name} store handler must not return values.`);
        }
      }
      else {
        ddb.warning(`REALTIME_ACTION: '${realtimeAction.constructor.name} - ACTION: ${action.constructor.name}(${this._logParams(action.params).join(', ')})' is not handled in the Store: '${this.constructor.name}'`);
      }
      this.dispatching.action = null;
      this.dispatching.on = false;
    });
    this._updateState();
  }
  
  dispatchRealtimeMessageAction(realtimeMessageAction) {
    const handler = Reflect.get(this, realtimeMessageAction.onRealtimeName);
    if(typeof handler === 'function') {
      if(undefined !== Reflect.apply(handler, this, [realtimeMessageAction])) {
        ddb.warning(`${this.constructor.name} store handler must not return values.`);
      }
      if(this.state !== this.stateCurrent) {
        this.stateCurrent = this.state;
        this.listeners.forEach((listener, index) => {
          listener();
        });
      }
    }
  }
  
  _sendRequest(realtimeAction, sessionId) {
    realtimeAction.setIds(GuidGenerator.create(), sessionId);
    this.workerCoreProtocol.sendRequest(realtimeAction.actionRequest, null);
    this.requests.set(realtimeAction.actionRequest.id, (response) => {
      realtimeAction.setResponses(response.responses);
      this.dispatchRealtimeAction(realtimeAction);
    });
  }
}
