
'use strict';

import StoreBase from './store-base';
import Action from '../communication/action';
import DataFetcher from '../communication/data-fetcher';


export default class StoreBaseData extends StoreBase {
  static waitOnceQueue = new Map();
  static waitOnQueue = new Map();
  static registered = new Set();

  constructor(state) {
    super(state);
  }
  
  once(dataActionName, cb, useRegistered = false) {
    if(!Array.isArray(dataActionName)) {
      if(useRegistered && StoreBaseData.registered.has(dataActionName)) {
        setTimeout(() => {
          cb();
        });
      }
      else {
        const cbs = StoreBaseData.waitOnceQueue.get(dataActionName);
        if(undefined !== cbs) {
          cbs.push(cb);
        }
        else {
          StoreBaseData.waitOnceQueue.set(dataActionName, [cb]);
        }
      }
    }
    else {
      const eventStatus = new Set();
      dataActionName.forEach((e) => {
        if(useRegistered && StoreBaseData.registered.has(dataActionName)) {
          eventStatus.add(dataActionName);
          const cbs = StoreBaseData.waitOnceQueue.get(dataActionName);
          if(undefined !== cbs) {
            cbs.push({
              cb: cb,
              eventStatus: eventStatus
            });
          }
          else {
            StoreBaseData.waitOnceQueue.set(dataActionName, [{
              cb: cb,
              eventStatus: eventStatus
            }]);
          }
        }
      });
      if(0 === eventStatus.size()) {
        setTimeout(() => {
          cb();
        });
      }
    }
  }
  
  on(dataActionName, cb) {
    const cbs = StoreBaseData.waitOnQueue.get(dataActionName);
    if(undefined !== cbs) {
      cbs.push(cb);
    }
    else {
      StoreBaseData.waitOnQueue.set(dataActionName, [cb]);
    }
  }
    
  sendDataAction(dataAction) {
    if(this.dispatching.on) {
      dataAction.actions.forEach((action) => {
        if(null !== this.dispatching.action) {
          action.setHistoryObject(this.dispatching.action);
        }
      });
    }
    DataFetcher.send(dataAction, this, () => {
      const dataActionName = dataAction.constructor.name;
      if(!StoreBaseData.registered.has(dataActionName)) {
        StoreBaseData.registered.add(dataActionName);
      }
      if(0 !== StoreBaseData.waitOnceQueue.size) {
        const cbs = StoreBaseData.waitOnceQueue.get(dataActionName);
        if(undefined !== cbs) {
          cbs.forEach((cb) => {
            if('function' === typeof cb) {
              cb(this);
            }
            else if('object' === typeof cb) {
              cb.eventStatus.has(dataActionName);
              cb.eventStatus.delete(dataActionName);
              if(0 === cb.eventStatus) {
                cb.cb(this);
              }
            }
          });
          StoreBaseData.waitOnceQueue.delete(dataActionName);
        }
      }
      if(0 !== StoreBaseData.waitOnQueue.size) {
        const cbs = StoreBaseData.waitOnQueue.get(dataActionName);
        if(undefined !== cbs) {
          cbs.forEach((cb) => {
            cb(this);
          });
        }
      }
    });
  }
  
  dispatchDataAction(dataAction, cb) {
    dataAction.actions.forEach((action) => {
      this.dispatching.action = action;
      this.dispatching.on = true;
      const handler = Reflect.get(this, `onData${action.constructor.name}`);
      if(typeof handler === 'function') {
        if(undefined !== Reflect.apply(handler, this, [action])) {
          ddb.warning(`${this.constructor.name} store handler must not return values.`);
        }
      }
      else {
        ddb.warning(`DATA_ACTION: '${dataAction.constructor.name} - ACTION: ${action.constructor.name}(${this._logParams(action.params).join(', ')})' is not handled in the Store: '${this.constructor.name}'`);
      }
      this.dispatching.action = null;
      this.dispatching.on = false;
    });
    this._updateState();
    if(this.onStateUpdate) {
      this.onStateUpdate();
    }
    dataAction.actions.forEach((action) => {
      if(action.onDataResponse) {
        action.onDataResponse(action.getResponse());
      }
    });
    cb();
  }
}
