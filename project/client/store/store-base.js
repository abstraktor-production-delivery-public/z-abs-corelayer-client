
'use strict';

import Action from '../communication/action';
import StateUpdater from '../state/state-updater';
import Verbose from '../state/verbose';


export default class StoreBase {
  static listenerId = 0;
  
  static SUCCESS = 'success';
  static FAILURE = 'failure';
  static ERROR = 'error';
  
  static WAIT_ONCE = 0;
  static WAIT_ON = 1;
  
  constructor(state) {
    this.state = state;
    this.stateCurrent = this.state;
    this.listeners = new Map();
    this.verbose = null;//new Verbose(false);
    this.stateUpdater = new StateUpdater(this.constructor.name, this.verbose);
    this.dispatching = {
      action: null,
      on: false
    }
    this.routerContext = null;
  }
  
  init() {}
  
  setContext(context) {
    if(!this.routerContext && 'RouterContext' === context.name) {
      this.routerContext = context;
    }
  }
  
  history(href, options, state) {
    if(this.routerContext) {
      this.routerContext.history(href, options, state);
    }
    else {
      ddb.warning(`No history, ${this.constructor.name} is not connected to 'RouterContext'.`);
    }
  }
  
  static export(obj) {
    let instance = null;
    const handlerGetFunc = (target, property) => {
      const g = instance[property];
      if(undefined !== g) {
        return instance[property];
      }
      else {
        return target[property];
      }
    };
    const handler = {
      get: (target, property) => {
        if('__esModule' === property) {
          return undefined;
        }
        else {
          if(!instance) {
            instance = new target();
            if(instance.onInit) {
              instance.renderOnInit();
            }
            handler.get = handlerGetFunc;
          }
          return handlerGetFunc(target, property);
        }
      },
      set(target, prop, value) {
        if(instance) {
          return Reflect.set(instance, prop, value);
        }
        else {
          target[prop] = value;
		      return true;
        }
      }
    };
    return new Proxy(obj, handler);
  }
  
  renderOnInit() {
    if(window.abstractorReleaseData) {
      this.onInit();
    }
    else {
      setTimeout(() => {
        this.renderOnInit();
      });
    }
  }
  
  getState() {
    return this.state;
  }
  
  addListener(listener) {
    const id = ++StoreBase.listenerId;
    this.listeners.set(id, listener);
    return {
      remove: () => {
        this.listeners.delete(id);
      }
    };
  }
  
  updateState(execs) {
    if(!this.dispatching.on) {
      ddb.error('No update context');
    }
    this.state = this.stateUpdater.updateState(execs, this.state, this.stateCurrent);
  }
  
  updateContext(cb) {
    this.dispatching.on = true;
    cb();
    this.dispatching.on = false;
    this._updateState();
  }
  
  shallowCopy(object) {
    return this.stateUpdater.shallowCopy(object);
  }
  
  deepCopy(object) {
    return this.stateUpdater.deepCopy(object);
  }
  
  dispatchAction(action) {
    const handler = Reflect.get(this, `on${action.constructor.name}`);
    if(typeof handler === 'function') {
      this.dispatching.action = action;
      this.dispatching.on = true;
      if(undefined !== Reflect.apply(handler, this, [action])) {
        ddb.warning(`${this.constructor.name} store handler must not return values.`);
      }
      this.dispatching.action = null;
      this.dispatching.on = false;
      this._updateState();
      if(this.onStateUpdate) {
        this.onStateUpdate();
      }
    }
    else {
      ddb.warning(`ACTION: '${action.constructor.name}(${this._logParams(action.params).join(', ')})' is not handled in the Store: '${this.constructor.name}'`);
    }
  }
  
  _updateState() {
    if(this.state !== this.stateCurrent) {
      this.stateCurrent = this.state;
      this.listeners.forEach((listener, index) => {
        listener();
      });
    }
  }
  
  _logParams(params) {
    if(undefined === params) {
      return [];
    }
    return params.map((param, index) => {
      if(param && typeof param === 'string') {
        return `'${param}'`;
      }
      else if(Array.isArray(param)) {
        return `[${this._logParams(param).join(', ')}]`;
      }
      else {
        return param;
      }
    });
  }
}
