
'use strict';

const WorkerThreadWebsocket = requireAsBundle('./worker-thread-websocket');
import WorkerMain from './worker-main';


class WorkerMainWebsocket extends WorkerMain {
  constructor(mainInterface, threadName, ...requires) {
    super(mainInterface, threadName, WorkerThreadWebsocket, ...requires);
  }
  
  static WS_CONNECT = 10000;
  static WS_DISCONNECT = 10001;
  static WS_CONNECTED = 10100;
  static WS_DISCONNECTED = 10101;
  static WS_SEND = 10102;
  static WS_ERROR = 10103;
  
  wsConnect(uri) {
    this.worker.postMessage([WorkerMainWebsocket.WS_CONNECT, uri]);
  }
  
  wsDisconnect() {
    this.worker.postMessage([WorkerMainWebsocket.WS_DISCONNECT]);
  }
  
  wsSend(msg) {
    this.worker.postMessage([WorkerMain.WS_SEND, msg]);
  }
  
  wsSendTransfer(transferList, msg) {
    this.worker.postMessage([WorkerMain.WS_SEND, msg], transferList);
  }
    
  onCmd(cmd, ...params) {
    switch(cmd) {
      case WorkerMainWebsocket.WS_CONNECTED: {
        if(this.mainInterface.onConnected) {
          this.mainInterface.onConnected(...params);
        }
        break;
      }
      case WorkerMainWebsocket.WS_DISCONNECTED: {
        if(this.mainInterface.onDisconnected) {
          this.mainInterface.onDisconnected(...params);
        }
        break;
      }
      case WorkerMainWebsocket.WS_ERROR: {
        if(this.mainInterface.onError) {
          this.mainInterface.onError(...params);
        }
        break;
      }
      default: {
        super.onCmd(cmd, ...params);
      }
    }
  }
}


module.exports = WorkerMainWebsocket;
