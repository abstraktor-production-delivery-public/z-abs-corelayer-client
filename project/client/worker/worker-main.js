
'use strict';

const WorkerThread = requireAsBundle('./worker-thread');
const Os = require('os');


class WorkerMain {
  static id = 0;
  
  constructor(mainInterface, threadName, ...requires) {
    this.mainInterface = mainInterface;
    this.threadName = threadName;
    requires.unshift(WorkerThread);
    this.workerString = requires.join(Os.EOL);
    this.workerString += Os.EOL + `const _thread = new ${threadName}(${++WorkerMain.id});` + Os.EOL;
    this.workerString += `_thread.run();` + Os.EOL;
    this.worker = null;
  }
  
  static WORKER_STARTED = 0;
  static WORKER_INITIALIZED = 1;
  static WORKER_STOPPED = 2;
  static WORKER_INIT = 0;
  static WORKER_EXIT = 1;
  static WORKER_MESSAGE = 1000;
    
  start() {
    const url = URL.createObjectURL(new Blob([this.workerString], {type : 'text/javascript'}));
    this.worker = new Worker(url);
    this.worker.onmessage = (e) => {
      const cmd = e.data.shift();
      const params = e.data;
      this.onCmd(cmd, ...params);
    };
    URL.revokeObjectURL(url);
  }
  
  init(...params) {
    this.worker.postMessage([WorkerMain.WORKER_INIT, ...params]);
  }
  
  stop() {
    this.worker.postMessage([WorkerMain.WORKER_EXIT]);
  }
  
  postMessage(...params) {
    this.worker.postMessage([WorkerMain.WORKER_MESSAGE, ...params]);
  }
  
  postMessageTransfer(transferList, ...params) {
    this.worker.postMessage([WorkerMain.WORKER_MESSAGE, ...params], transferList);
  }
  
  onCmd(cmd, ...params) {
    switch(cmd) {
      case WorkerMain.WORKER_STARTED: {
        if(this.mainInterface.onStarted) {
          this.mainInterface.onStarted(...params);
        }
        break;
      }
      case WorkerMain.WORKER_INITIALIZED: {
        if(this.mainInterface.onInitialized) {
          this.mainInterface.onInitialized(...params);
        }
        break;
      }
      case WorkerMain.WORKER_STOPPED: {
        if(this.mainInterface.onStopped) {
          this.mainInterface.onStopped(...params);
        }
        break;
      }
      case WorkerMain.WORKER_MESSAGE: {
        if(this.mainInterface.onMessage) {
          this.mainInterface.onMessage(...params);
        }
        break;
      }
    }
  }
}


module.exports = WorkerMain;
