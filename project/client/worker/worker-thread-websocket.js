
'use strict';

const WorkerThread = require('./worker-thread');


class WorkerThreadWebsocket extends WorkerThread {
  constructor(id) {
    super(id);
    this.webSocket = null;
  }
  
  static WS_CONNECT = 10000;
  static WS_DISCONNECT = 10001;
  static WS_CONNECTED = 10100;
  static WS_DISCONNECTED = 10101;
  static WS_SEND = 10102;
  static WS_ERROR = 10103;
  
  onCmd(cmd, ...params) {
    switch(cmd) {
      case WorkerThreadWebsocket.WS_SEND: {
        this.onWsSend(...params);
        break;
      }
      case WorkerThreadWebsocket.WS_CONNECT: {
        this.onWsConnect(...params);
        break;
      }
      case WorkerThreadWebsocket.WS_DISCONNECT: {
        this.onWsDisconnect(...params);
        break;
      }
      default: {
        super.onCmd(cmd, ...params);
      }
    }
  }
  
  onWsSend(msg) {
    if(this.webSocket) {
      this.webSocket.send(msg);
    }
  }
  
  onWsConnect(uri, name) {
    if(!this.webSocket) {
      this.webSocket = new WebSocket(uri);
      this.webSocket.binaryType = "arraybuffer";
      this.webSocket.onopen = () => {
        this.webSocket.onmessage = this._onWsMessage.bind(this);
        postMessage([WorkerThreadWebsocket.WS_CONNECTED]);
      };
      
      this.webSocket.onerror = (e) => {
        postMessage([WorkerThreadWebsocket.WS_ERROR]);
      };
    
      this.webSocket.onclose = (e) => {
        this.webSocket = null;
        postMessage([WorkerThreadWebsocket.WS_DISCONNECTED]);
      };
    }
  }
  
  onWsDisconnect() {
    if(this.webSocket) {
      this.webSocket.close(); 
      this.webSocket = null;
    }
  }
  
  _onWsMessage(msg) {
    if(this.onWsMessage) {
      this.onWsMessage(msg.data);
    }
  }
}


module.exports = WorkerThreadWebsocket;
