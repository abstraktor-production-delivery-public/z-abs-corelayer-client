
'use strict';

const WorkerThreadWebsocket = require('./worker-thread-websocket');


class WorkerThreadCoreProtocol extends WorkerThreadWebsocket {
  constructor(id) {
    super(id);
    this.deserializer = new Deserializer(Decoder, null);
  }
  
  static CORE_PROTOCOL_SEND_MESSAGE = 10200;
  static CORE_PROTOCOL_REQUEST = 10201;
  static CORE_PROTOCOL_RECEIVE_MESSAGE = 10202;
  static CORE_PROTOCOL_RECEIVE_RESPONSE = 10203;
  
  static INIT = 1;
    
  onCmd(cmd, ...params) {
    switch(cmd) {
      case WorkerThreadCoreProtocol.CORE_PROTOCOL_SEND_MESSAGE: {
        this.onSendRealtimeMessage(...params);
        break;
      }
      case WorkerThreadCoreProtocol.CORE_PROTOCOL_REQUEST: {
        this.onSendRequest(...params);
        break;
      }
      default: {
        super.onCmd(cmd, ...params);
      }
    }
  }
    
  onSendRealtimeMessage(...params) {
    this.onWsSend(...params);  
  }
  
  onSendRequest(request) {
    this.onWsSend(request);
  }
  
  onWsMessage(buffer) {
    const coreMessage = this.deserializer.do(buffer);
    if(coreMessage) {
      if(!coreMessage.isBinary) {
        let transferList;
        if(coreMessage.hasBuffers) {
          transferList = [];
          coreMessage.dataBuffers.forEach((dataBuffer) => {
            transferList.push(dataBuffer.buffer);
          });
        }
        postMessage([WorkerThreadCoreProtocol.CORE_PROTOCOL_RECEIVE_MESSAGE, coreMessage]);
      }
      else {
        const transferList = [coreMessage.bin.buffer];
        if(coreMessage.hasTextCache) {
          transferList.push(coreMessage.cachedTextBuffer.buffer);
        }
        if(coreMessage.hasBuffers) {
          coreMessage.dataBuffers.forEach((dataBuffer) => {
            transferList.push(dataBuffer.buffer);
          });
        }
        postMessage([WorkerThreadCoreProtocol.CORE_PROTOCOL_RECEIVE_MESSAGE, coreMessage], transferList);
      }
      this.onWsMessage(null);
    }
  }
}


module.exports = WorkerThreadCoreProtocol;
