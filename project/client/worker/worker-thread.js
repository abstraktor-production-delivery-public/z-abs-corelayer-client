
'use strict';


class WorkerThread {
  constructor(id) {
    this.id = id;
  }
  
  static WORKER_STARTED = 0;
  static WORKER_INITIALIZED = 1;
  static WORKER_STOPPED = 2;
  static WORKER_INIT = 0;
  static WORKER_EXIT = 1;
  static WORKER_MESSAGE = 1000;
  
  run() {
    onmessage = (e) => {
      const cmd = e.data.shift();
      const params = e.data;
      this.onCmd(cmd, ...params);
    };
    postMessage([WorkerThread.WORKER_STARTED]);
  }
  
  onCmd(cmd, ...params) {
    switch(cmd) {
      case WorkerThread.WORKER_INIT: {
        if(this.onInit) {
          this.onInit(...params, () => {
            postMessage([WorkerThread.WORKER_INITIALIZED]);
          });
        }
        else {
          postMessage([WorkerThread.WORKER_INITIALIZED]);
        }
        break;
      }
      case WorkerThread.WORKER_EXIT: {
        if(this.onExit) {
          this.onExit(...params, () => {
            self.close();
            postMessage([WorkerThread.WORKER_STOPPED]);
          });
        }
        else {
          self.close();
          postMessage([WorkerThread.WORKER_STOPPED]);
        }
        break;
      }
      case WorkerThread.WORKER_MESSAGE: {
        if(this.onMessage) {
          this.onMessage(...params);
        }
        break;
      }
    }
  }
  
  postMessage(...params) {
    postMessage([WorkerThread.WORKER_MESSAGE, ...params]);
  }
  
  postMessageTransfer(transferList, ...params) {
    postMessage([WorkerThread.WORKER_MESSAGE, ...params], transferList);
  }
}


module.exports = WorkerThread;
