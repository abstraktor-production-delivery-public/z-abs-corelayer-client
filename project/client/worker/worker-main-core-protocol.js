
'use strict';

const WorkerThreadCoreProtocol = requireAsBundle('./worker-thread-core-protocol');
import WorkerMainWebsocket from './worker-main-websocket';
import Encoder from '../communication/core-protocol/encoder';
import Serializer from 'z-abs-corelayer-cs/clientServer/communication/core-protocol/serializer';


class WorkerMainCoreProtocol extends WorkerMainWebsocket {
  constructor(mainInterface, threadName, ...requires) {
    super(mainInterface, threadName, WorkerThreadCoreProtocol, ...requires);
    this.serverSerializer = new Serializer(Encoder); // TODO: add this.serverSerializer.clear();
  }
  
  static CORE_PROTOCOL_SEND_MESSAGE = 10200;
  static CORE_PROTOCOL_REQUEST = 10201;
  static CORE_PROTOCOL_RECEIVE_MESSAGE = 10202;
  static CORE_PROTOCOL_RECEIVE_RESPONSE = 10203;
  
  sendRealtimeMessage(msg, dataBuffers) {
    const buffers = this.serverSerializer.do(msg, dataBuffers, false);
    this.worker.postMessage([WorkerMainCoreProtocol.CORE_PROTOCOL_SEND_MESSAGE, ...buffers], buffers);
  }
  
  sendRequest(request, dataBuffers) {
    const buffers = this.serverSerializer.do(request, dataBuffers, false);
    this.worker.postMessage([WorkerMainCoreProtocol.CORE_PROTOCOL_REQUEST, ...buffers], buffers);
  }
  
  onCmd(cmd, ...params) {
    switch(cmd) {
      case WorkerMainCoreProtocol.CORE_PROTOCOL_RECEIVE_MESSAGE: {
        if(this.mainInterface.onCoreProtocolMessage) {
          this.mainInterface.onCoreProtocolMessage(...params);
        }
        break;
      }
      default: {
        super.onCmd(cmd, ...params);
      }
    }
  }
}


module.exports = WorkerMainCoreProtocol;
