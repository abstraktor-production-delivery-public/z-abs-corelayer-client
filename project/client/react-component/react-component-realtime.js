
'use strict';

import ReactComponentStore from './react-component-store';
import React from 'react';


export default class ReactComponentRealtime extends ReactComponentStore {
  static realtimeGroups = [];
  static realtimeObjectsToRender = [];
  static realtimeCallbacks = [];
  static updating = false;
  
  static OBJECT_TYPE_TEXT = 0;
  static OBJECT_TYPE_NODE = 1;
  
  constructor(props, stores, state) {
    super(props, stores, state);
    this.realtimeRemovers = [];
    if(!stores || 0 === stores.length) {
      ddb.warning(`In ${this.constructor.name} extend ReactComponentBase, when there are no stores, instead of ReactComponentRealtime.`);
    }
    else {
      if(!stores.some((store) => {
        return store.realtime;
      })) {
        ddb.warning(`In ${this.constructor.name} extend ReactComponentStore, when there are no stores supporting realtime, instead of ReactComponentRealtime.`);
      }
    }
    this.appDeserializer = null;
    stores.forEach((store) => {
      if(store.appDeserializer) {
        this.appDeserializer = store.appDeserializer;
      }
    });
  }
  
  componentDidMount() {
    super.componentDidMount();
    this.stores.forEach((store) => {
      if(typeof store.addRealtimeListener === 'function') {
        this.realtimeRemovers.push(store.addRealtimeListener(this.constructor.name, (message) => {
          this.onRealtimeMessage(message);
        }));
      }
    });
  }
  
  componentWillUnmount() {
    this.realtimeRemovers.forEach((realtimeRemover) => {
      realtimeRemover.remove();
    });
    super.componentWillUnmount();
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    if(!this.cbIsRealtime()) {
      return super.shouldComponentUpdate(nextProps, nextState);
    }
    else {
      if(this.realtimeUpdate) {
        this.realtimeUpdate(nextProps, nextState);
      }
      return false;
    }
  }
  
  cbIsRealtime() {
    return false;
  }
  
  onRealtimeMessage(msg) {}
  
  renderRealtime(cb, ...params) {
    if(cb) {
      ReactComponentRealtime.realtimeCallbacks.push({cb, params});
    }
    ReactComponentRealtime.renderRealtimeFrame();
  }
      
  addRealtimeFrameGroup() {
    const groupData = {
      id: ReactComponentRealtime.realtimeGroups.length,
      frames: new Map()
    };
    ReactComponentRealtime.realtimeGroups.push(groupData);
    return groupData;
  }
  
  removeRealtimeFrameGroup(groupData) {
    if(ReactComponentRealtime.realtimeGroups.length - 1 !== groupData.id) {
      const id = groupData.id;
      ReactComponentRealtime.realtimeGroups[id] = ReactComponentRealtime.realtimeGroups.pop();
      ReactComponentRealtime.realtimeGroups[id].id = id;
    }
    else {
      ReactComponentRealtime.realtimeGroups.pop();
    }
  }
  
  addRealtimeFrame(name, groupData, value) {
    ReactComponentRealtime.realtimeGroups[groupData.id].frames.set(name, {
      value: value,
      renderer: null
    });
  }
  
  replaceRealtimeFrameText(name, groupData, nextText, css=null) {
    const current = ReactComponentRealtime.realtimeGroups[groupData.id].frames.get(name);
    if(!current.renderer) {
      current.renderer = {
        objectType: ReactComponentRealtime.OBJECT_TYPE_TEXT,
        name: name,
        groupData: groupData,
        css: css,
        nextText: nextText,
        next: null
      };
      ReactComponentRealtime.realtimeObjectsToRender.push(current.renderer);
      ReactComponentRealtime.renderRealtimeFrame();
    }
    else {
      current.renderer.css = css;
      current.renderer.nextText = nextText;
      current.renderer.next = null;
    }
  }
  
  replaceRealtimeFrameObject(name, groupData, nextObject, css=null) {
    const current = ReactComponentRealtime.realtimeGroups[groupData.id].frames.get(name);
    if(!current.renderer) {
      current.renderer = {
        objectType: ReactComponentRealtime.OBJECT_TYPE_NODE,
        name: name,
        groupData: groupData,
        css: css,
        next: nextObject
      };
      ReactComponentRealtime.realtimeObjectsToRender.push(current.renderer);
      ReactComponentRealtime.renderRealtimeFrame();
    }
    else {
      current.renderer.css = css;
      current.renderer.next = nextObject;
    }
  }
  
  static renderAnimationFrame(timestamp) {
    const length = ReactComponentRealtime.realtimeObjectsToRender.length;
    for(let i = 0; i < length; ++i) {
      const qO = ReactComponentRealtime.realtimeObjectsToRender[i];
      const current = ReactComponentRealtime.realtimeGroups[qO.groupData.id].frames.get(qO.name);
      if(ReactComponentRealtime.OBJECT_TYPE_NODE === qO.objectType) {
        if(qO.css) {
          qO.next.classList.remove(...qO.next.classList);
          qO.next.classList.add(qO.css);
        }
      }
      else if(ReactComponentRealtime.OBJECT_TYPE_TEXT === qO.objectType) {
        qO.next = document.createTextNode(qO.nextText);
        if(qO.css) {
          qO.next.parentNode.classList.remove(...qO.next.parentNode.classList);
          qO.next.parentNode.classList.add(qO.css);
        }
      }
      current.value.parentNode.replaceChild(qO.next, current.value);
      current.value = qO.next;
      current.renderer = null;
    };
    ReactComponentRealtime.realtimeObjectsToRender = [];
    const cbLength = ReactComponentRealtime.realtimeCallbacks.length;
    for(let i = 0; i < cbLength; ++i) {
      const cbData = ReactComponentRealtime.realtimeCallbacks[i];
      if(cbData.params) {
        cbData.cb(...cbData.params);
      }
      else {
        cbData.cb();
      }
    }
    ReactComponentRealtime.realtimeCallbacks = [];
    ReactComponentRealtime.updating = false;
  }
  
  static renderRealtimeFrame() {
    if(!ReactComponentRealtime.updating) {
      ReactComponentRealtime.updating = true;
      requestAnimationFrame(ReactComponentRealtime.renderAnimationFrame);
    }
  }
}

