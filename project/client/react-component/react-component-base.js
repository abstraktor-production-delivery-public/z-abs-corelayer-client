
'use strict';

import StateUpdater from '../state/state-updater';
import Verbose from '../state/verbose';
import Context from './context';
import React from 'react';


export default class ReactComponentBase extends React.Component {
  static TitleSize = 16;
  static popoverId = 0;
  static logId = 0;
  static shouldUpdateId = 0;
  
  constructor(props, state={}) {
    super(props);
    this.state = state;
    this.stateNext = state;
    this.myContext = new Proxy(Context, {
      get(target, property, receiver) {
        return target.get(property);
      },
      set(target, property, value, receiver) {
        target.set(property, value, this);
        return true;
      }
    });
    this.titleFunc = null;
    this.verbose = null; //new Verbose(true)
    this.stateUpdater = new StateUpdater(this.constructor.name, this.verbose);
    this.services = new Map();
  }
  
  componentDidMount() {
    this.stateNext = this.state;
    try {
      if(this.didMount) {
        this.didMount();
      }
      if(this.verbose) {
        ddb.info(`${++ReactComponentBase.logId}: didMount: ${this.constructor.name}`);
      }
    }
    catch(err) {
      this._logError(`didMount: ${this.constructor.name}`, err);
    }
    this._setTitle();
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    if(!this.shouldUpdate) {
      ddb.warning(`${this.constructor.name}.shouldUpdate is not implemented.`);
      return true;
    }
    try {
      if(!this.verbose) {
        return this.shouldUpdate(nextProps, nextState);
      }
      else {
        this.verbose.clear();
        if(this.shouldUpdate(nextProps, nextState)) {
          ddb.info(`${++ReactComponentBase.shouldUpdateId}: ${this.constructor.name}.shouldUpdate: ${this.verbose.compareResult}`);
          return true;
        }
        else {
          return false;
        }
      }
    }
    catch(err) {
      this._logError(`${++ReactComponentBase.logId}: shouldUpdate: ${this.constructor.name}`, err);
      return true;
    }
  }
  
  getSnapshotBeforeUpdate(prevProps, prevState) {
    try {
      if(this.snapshotBeforeUpdate) {
        const snapshot = this.snapshotBeforeUpdate(prevProps, prevState);
        if(this.verbose) {
          ddb.info(`${++ReactComponentBase.logId}: snapshotBeforeUpdate: ${this.constructor.name}`);
        }
        return snapshot;
      }
      else {
        return null;
      }
    }
    catch(err) {
      this._logError(`${++ReactComponentBase.logId}: snapshotBeforeUpdate: ${this.constructor.name}`, err);
    }
    return null;
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    this.stateNext = this.state;
    try {
      if(this.didUpdate) {
        this.didUpdate(prevProps, prevState, snapshot);
        if(this.verbose) {
          ddb.info(`${++ReactComponentBase.logId}: didUpdate: ${this.constructor.name}`);
        }
      }
    }
    catch(err) {
      this._logError(`${++ReactComponentBase.logId}: didUpdate: ${this.constructor.name}`, err);
    }
    this._setTitle();
  }
  
  componentWillUnmount() {
    try {
      if(this.willUnmount) {
        this.willUnmount();
      }
    }
    catch(err) {
      this._logError(`${++ReactComponentBase.logId}: willUnmount: ${this.constructor.name}`, err);
    }
  }
  
  shallowCompare(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompare(thisCompare, nextCompare);
  }
  
  shallowCompareObjectValues(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompareObjectValues(thisCompare, nextCompare);
  }
  
  shallowCompareArrayValues(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompareArrayValues(thisCompare, nextCompare);
  }
  
  shallowCompareMapValues(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompareMapValues(thisCompare, nextCompare);
  }
  
  shallowCompareSetValues(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompareSetValues(thisCompare, nextCompare);
  }
  
  deepCompare(thisCompare, nextCompare) {
    return this.stateUpdater.deepCompare(thisCompare, nextCompare);
  }
  
  setState(nextState, callback) {
    ddb.warning(this.constructor.name + '.setState is forbidden. Use updateState instead.');
    super.setState(nextState, callback);
  }
  
  updateState(execs, cb) {
    this.stateNext = this.stateUpdater.updateState(execs, this.stateNext, this.state);
    super.setState(this.stateNext, cb);
  }
  
  shallowCopy(object) {
    return this.stateUpdater.shallowCopy(object);
  }
  
  deepCopy(object) {
    return this.stateUpdater.deepCopy(object);
  }
  
  classNameAttribute(attribute, className) {
    if(attribute) {
      return ` ${className}`;
    }
    else {
      return '';
    }
  }
  
  serviceExists(service) {
    const serviceExists1 = this.services.get(service);
    if(undefined === serviceExists1) {
      try {
        const serviceExists2 = !!require(`${service}/client/service-exists`);
        this.services.set(service, serviceExists2);
        return serviceExists2;
      }
      catch(e) {
        this.services.set(service, false);
        return false;
      }
    }
    else {
      return serviceExists1;
    }
  }
  
  _logError(member, err) {
    ddb.error(`Exception in ${this.constructor.name}.${member}. Message: '${err.message}'`);
    if(undefined !== err.stack) {
      ddb.error(`Stack: '${err.stack}'`);
    }
  }
  
  setTitle(titleFunc) {
    this.titleFunc = titleFunc;
  }
  
  _setTitle() {
    if(this.titleFunc) {
      const title = this.titleFunc();
      if(title.isPath) {
        const parts = title.text.split('/');
        document.title = `${title.prefix ? title.prefix : ''}${parts[parts.length - 1]}`;
      }
      else {
        document.title = `${title.prefix ? title.prefix : abstractorReleaseData.appTitle} - ${title.text}`;
      }
    } 
  }
  
  theme(dark, className, ...classNames) {
    const classNameArray = [];
    const classNameModeArray = [];
    if(!Array.isArray(className)) {
      classNameArray.push(className);
      classNameModeArray.push(`${className}_${dark ? 'dark' : 'light'}`);
    }
    else {
      className.forEach((cName) => {
        classNameArray.push(cName);
        classNameModeArray.push(`${cName}_${dark ? 'dark' : 'light'}`);
      });
    }
    const mergedClasses = classNameArray.concat(classNameModeArray, classNames);
    return mergedClasses.join(' ');
  }
}

