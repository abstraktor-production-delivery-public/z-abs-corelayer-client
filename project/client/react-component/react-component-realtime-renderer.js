
'use strict';

import ReactComponentRealtime from './react-component-realtime';
import React from 'react';


export default class ReactComponentRealtimeRenderer extends ReactComponentRealtime {
  static listenerCurrentId = 0;
  static updating = false;
  static activeListeners = new Map();
  
  constructor(props, stores, dataSize, state) {
    super(props, stores, state);
    this.data = new Array(dataSize).fill(false, 0, dataSize);
    this.id = ++ReactComponentRealtimeRenderer.listenerCurrentId;
    this.funcRenderRealtimeFrame = this.onRenderRealtimeFrame.bind(this);
  }
    
  renderRealtimeData(data, cb, ...params) {
    let renderObject = null;
    if(!ReactComponentRealtimeRenderer.activeListeners.has(this.id)) {
      const data = Object.assign({}, this.data);
      renderObject = {
        cbFunction: this.funcRenderRealtimeFrame,
        data: [...this.data],
        cbs: cb ? [{cb, params}] : []
      };
      ReactComponentRealtimeRenderer.activeListeners.set(this.id, renderObject);
    }
    else {
      renderObject = ReactComponentRealtimeRenderer.activeListeners.get(this.id);
      if(cb) {
        renderObject.cbs.push({cb, params});
      }
    }
    if(data) {
      for(let i = 0; i < data.length; ++i) {
        renderObject.data[i] ||= data[i];
      }
    }
    ReactComponentRealtimeRenderer.renderRealtimeRenderer();
  }
  
  static renderRealtimeRendererFrame(timestamp) {
    ReactComponentRealtimeRenderer.activeListeners.forEach((activeListener) => {
      activeListener.cbFunction(timestamp, ...activeListener.data);
      activeListener.cbs.forEach((data) => {
        if(data.cb) {
          if(data.params) {
            data.cb(timestamp, ...data.params)
          }
          else {
            data.cb(timestamp);
          }
        }
      });
      ReactComponentRealtimeRenderer.renderRealtimeRenderer();
    });
    ReactComponentRealtimeRenderer.activeListeners.clear();
    ReactComponentRealtimeRenderer.updating = false;
  }
  
  static renderRealtimeRenderer() {
    if(!ReactComponentRealtimeRenderer.updating) {
      ReactComponentRealtimeRenderer.updating = true;
      requestAnimationFrame(ReactComponentRealtimeRenderer.renderRealtimeRendererFrame);
    }
  }
}

