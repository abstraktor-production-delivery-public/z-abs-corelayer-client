
'use strict';

import ReactComponentBase from '../react-component/react-component-base';
import React from 'react';


export default class NotFound extends ReactComponentBase {
  constructor(props) {
    super(props);
    /*this.setTitle(() => {
      return {
        isPath: this.props._,
        text: this.props._ ? this.props._[0] : 'Not Found',
        prefix: 'NF: '
      };
    });*/
  }
  
  shouldUpdate(nextProps, nextState) {
    return true
  }
  
  render() {
    return (
      <div className="not_found_page">
        <img id="not_found_page" src="/abs-images/svg/AbstraktorLogo.svg" alt="Abstraktor Logo"></img>
        <h1 className="not_found_page">
          Page Not Found
        </h1>
        <br />
        <h4>
          Links to Stacks, Actors, ConnectionWorkers, Test Cases and Test Suites are available from the ActorJs tool. You can download ActorJs from npm <a href="https://www.npmjs.com/package/@abstraktor/actorjs">@abstraktor/actorjs</a>.
        </h4>
      </div>
    );
  }
}
