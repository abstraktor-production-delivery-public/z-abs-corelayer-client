
'use strict';


export default class HelperTab {
  constructor(tabNames) {
    this.tabNames = new Map();
    this.tabRoutes = new Map();
    this.tabIndices = new Map();
    if(undefined !== tabNames) {
      tabNames.forEach((tabName) => {
        this.add(tabName);
      });
    }
  }
  
  add(tabName) {
    let index = this.tabNames.size;
    this.tabNames.set(index, tabName);
    this.tabRoutes.set(index, tabName.toLowerCase().replace(' ', '-'));
    this.tabIndices.set(tabName.toLowerCase().replace(' ', '-'), index);
  }
  
  getIndex(tabName) {
    if(undefined !== tabName) {
      return this.tabIndices.get(tabName.toLowerCase().replace(' ', '-'));
    }
    else {
      return 0;
    }
  }
  
  getTabName(index) {
    return this.tabNames.get(index);
  }
  
  getRoute(index) {
    return this.tabRoutes.get(index);
  }
}
