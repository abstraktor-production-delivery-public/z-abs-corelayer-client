
'use strict';

import Action from '../communication/action';
import DataAction from '../communication/data-action';


export class ActionDialogFileSet extends Action {
  constructor(path) {
    super(path);
  }
}

export class ActionDialogFileGet extends Action {
  constructor(path, rootName, pathFilters, existingFilters, type, projectType, plugin, workspaceName) {
    super(path, rootName, pathFilters, existingFilters, type, projectType, plugin, workspaceName);
  }
}

export class ActionDialogWorkspaceSet extends Action {
  constructor(workspace) {
    super(workspace);
  }
}

export class DataActionDialogFileGet extends DataAction {
  constructor(path, rootName, pathFilters, existingFilters, type, projectType, plugin, workspaceName) {
    super();
    this.addRequest(new ActionDialogFileGet(path, rootName, pathFilters, existingFilters, type, projectType, plugin, workspaceName), rootName, pathFilters, existingFilters, type, projectType, plugin, workspaceName);
  }
}
