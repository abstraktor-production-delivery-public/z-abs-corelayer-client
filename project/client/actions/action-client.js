
'use strict';

import Action from '../communication/action';
import DataAction from '../communication/data-action';


export class ActionClientSet extends Action {
  constructor() {
    super();
  }
}

export class DataActionClientGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionClientSet());
  }
}
