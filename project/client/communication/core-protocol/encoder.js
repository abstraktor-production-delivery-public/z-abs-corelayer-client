
'use strict';

import EncoderConst from 'z-abs-corelayer-cs/clientServer/communication/core-protocol/encoder-const';


class Encoder {
  static GuidSize = EncoderConst.GuidSize;
  static Int8Size = EncoderConst.Int8Size;
  static Int16Size = EncoderConst.Int16Size;
  static Int32Size = EncoderConst.Int32Size;
  static Int64Size = EncoderConst.Int64Size;
  static Uint8Size = EncoderConst.Uint8Size;
  static Uint16Size = EncoderConst.Uint16Size;
  static Uint32Size = EncoderConst.Uint32Size;
  static Uint64Size = EncoderConst.Uint64Size;
  static Float32Size = EncoderConst.Float32Size;
  static Float64Size = EncoderConst.Float64Size;
  static CtSize = EncoderConst.CtSize;
  static Uint8ArraySize = EncoderConst.Uint8ArraySize;
  
  static textEncoder = new TextEncoder();
  static ASCII = [
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
     0,  1,  2,  3,  4,  5,  6,  7,
     8,  9, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, -1
  ];
  
  constructor(textCache) {
    this.textCache = textCache;
    this.offset = 0;
    this.dataView = null;
    this.doCalculate = false;
  }
  
  clear() {
    this.dataView = null;
    this.offset = 0;
  }
  
  createBuffer(size) {
    const buffer = new ArrayBuffer(size);
    this.dataView = new DataView(buffer);
    return buffer;
  }
  
  calculate(doCalculate) {
    this.doCalculate = doCalculate;
    this.offset = 0;
  }
    
  calculateBytesFromString(s) {
    if(!s) {
      return 0;
    }
    const length = s.length;
    let stringBytes = 0;
    let ch = null;
    let notFirst = false;
    for(let i = 0; i < length; ++i) {
      try {
        ch = s.charCodeAt(i);
      }
      catch(e) {
        debugger;
      }
      if(ch >= 0xdc00 && ch <= 0xdfff) {
        if(notFirst && (ch >= 0xd800 && ch <= 0xdbff)) {
          ++stringBytes;
        }
        else {
          stringBytes += 3;
        }
      }
      else if(ch <= 0x7f) {
        stringBytes += 1;
      }
      else if(ch >= 0x80 && ch <= 0x7ff) {
        stringBytes += 2;
      }
      else if(ch >= 0x800 && ch <= 0xffff) {
        stringBytes += 3;
      }
      notFirst = true;
    }
    return stringBytes;
  }
  
  calculateBytesFromBin(b) {
    return b.byteLength;
  }
  
  calculateDynamicBytes(size) {
    if(size <= 253) {
      return 1;
    }
    else if(size <= 65535) {
      return 3;
    }
    else {
      return 5;
    }
  }
  
  getStringBytes(s) {
    const stringBytes = this.calculateBytesFromString(s);
    return stringBytes + this.calculateDynamicBytes(stringBytes);
  }
  
  getStringArrayBytes(array) {
    let size = this.calculateDynamicBytes(array.length);
    for(let i = 0; i < array.length; ++i) {
      size += this.getStringBytes(array[i]);
    }
    return size;
  }
  
  getGuidArrayBytes(array) {
    return this.calculateDynamicBytes(array.length) + (array.length * EncoderConst.GuidSize);
  }
  
  setDynamicBytes(size) {
    const sizeBytes = this.calculateDynamicBytes(size);
    if(1 === sizeBytes) {
      this.setUint8(size);
    }
    else if(3 === sizeBytes) {
      this.setUint8(254);
      this.setUint16(size);
    }
    else {
      this.setUint8(255);
      this.setUint32(size);
    }
  }
  
  setInt8(value) {
    if(!this.doCalculate) {
      this.dataView.setInt8(this.offset, value);
    }
    this.offset += Encoder.Int8Size;
  }
  
  setInt16(value) {
    if(!this.doCalculate) {
      this.dataView.setInt16(this.offset, value);
    }
    this.offset += Encoder.Int16Size;
  }
  
  setInt32(value) {
    if(!this.doCalculate) {
      this.dataView.setInt32(this.offset, value);
    }
    this.offset += Encoder.Int32Size;
  }
  
  setBigInt64(value) {
    if(!this.doCalculate) {
      this.dataView.setBigInt64(this.offset, value);
    }
    this.offset += Encoder.Int64Size;
  }
  
  setUint1_0(value, ready) {
    if(!this.doCalculate) {
      const v = 0x7f & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 7));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_1(value, ready) {
    if(!this.doCalculate) {
      const v = 0xbf & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 6));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
   setUint1_2(value, ready) {
    if(!this.doCalculate) {
      const v = 0xdf & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 5));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_3(value, ready) {
    if(!this.doCalculate) {
      const v = 0xef & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 4));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_4(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf7 & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 3));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_5(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfb & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 2));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_6(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfd & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value) << 1));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_7(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfe & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x1 & value)));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_0(value, ready) {
    const v = 0x3f & this.dataView.getUint8(this.offset);
    this.dataView.setUint8(this.offset, v + ((0x3 & value) << 6));
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_1(value, ready) {
    const v = 0xcf & this.dataView.getUint8(this.offset);
    this.dataView.setUint8(this.offset, v + ((0x3 & value) << 4));
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_2(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf3 & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0x3 & value) << 2));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_3(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfc & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + (0x3 & value));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint4_0(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + ((0xf & value) << 4));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint4_1(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf0 & this.dataView.getUint8(this.offset);
      this.dataView.setUint8(this.offset, v + (0xf & value));
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint8(value) {
    if(!this.doCalculate) {
      this.dataView.setUint8(this.offset, value);
    }
    this.offset += Encoder.Uint8Size;
  }
  
  setUint16(value) {
    if(!this.doCalculate) {
      this.dataView.setUint16(this.offset, value);
    }
    this.offset += Encoder.Uint16Size;
  }
  
  setUint32(value) {
    if(!this.doCalculate) {
      this.dataView.setUint32(this.offset, value);
    }
    this.offset += Encoder.Uint32Size;
  }
  
 setBigUint64(value) {
   if(!this.doCalculate) {
      this.dataView.setBigUint64(this.offset, value);
   }
    this.offset += Encoder.Uint64Size;
  }
  
  setFloat32(value) {
    if(!this.doCalculate) {
      this.dataView.setFloat32(this.offset, value);
    }
    this.offset += Encoder.Float32Size;
  }
  
  setFloat64(value) {
    if(!this.doCalculate) {
      this.dataView.setFloat64(this.offset, value);
    }
    this.offset += Encoder.Float64Size;
  }
  
  setBool1_0(value, ready) {
    this.setUint1_0(value ? 1 : 0, ready);
  }
  
  setBool1_1(value, ready) {
    this.setUint1_1(value ? 1 : 0, ready);
  }
  
  setBool1_2(value, ready) {
    this.setUint1_2(value ? 1 : 0, ready);
  }
  
  setBool1_3(value, ready) {
    this.setUint1_3(value ? 1 : 0, ready);
  }
  
  setBool1_4(value, ready) {
    this.setUint1_4(value ? 1 : 0, ready);
  }
  
  setBool1_5(value, ready) {
    this.setUint1_5(value ? 1 : 0, ready);
  }
  
  setBool1_6(value, ready) {
    this.setUint1_6(value ? 1 : 0, ready);
  }
  
  setBool1_7(value, ready) {
    this.setUint1_7(value ? 1 : 0, ready);
  }
  
  _setCt(value) {
    if(!this.doCalculate) {
      this.dataView.setInt16(this.offset, value);
    }
    this.offset += Encoder.CtSize;
  }
  
  setCtString(text, cachedTexts) {
    if(!this.doCalculate) {
      if(!text) {
        text = '';
      }
      const ctId = this.textCache.setExternal(text, cachedTexts);
      this._setCt(ctId);
    }
    else {
      this.offset += Encoder.CtSize;
    }
  }
  
  setCtStringArray(array, cachedTexts) {
    this.setUint16(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setCtString(array[i], cachedTexts);
    }
  }
  
  setCtStringInternal(text) {
    if(!this.doCalculate) {
      const ctId = this.textCache.setInternal(text);
      this._setCt(ctId);
    }
    else {
      this.offset += Encoder.CtSize;
    }
  }
  
  setCtStringArrayInternal(array) {
    this.setUint16(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setCtStringInternal(array[i]);
    }
  }
  
  setRawString(value, size) {
    if(!this.doCalculate) {
      if(0 !== size) {
        const result = Encoder.textEncoder.encodeInto(value, new Uint8Array(this.dataView.buffer, this.offset, size));
      }
    }
    this.offset += size;
  }
  
  setString(value) {
    const stringBytes = this.calculateBytesFromString(value);
    this.setDynamicBytes(stringBytes);
    this.setRawString(value, stringBytes);
  }
  
  setStringArray(array) {
    this.setDynamicBytes(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setString(array[i]);
    }
  }
  
  setRawBinary(value, size) {
    if(!this.doCalculate) {
      let srcArray = null;
      let srcDataView = null;
      if(value instanceof ArrayBuffer) {
        srcDataView = new DataView(value);
      }
      else {
        srcDataView = new DataView(value.buffer, value.byteOffset);
      }
      srcArray = new Uint8Array(srcDataView.buffer, srcDataView.byteOffset);
      const dstArray = new Uint8Array(this.dataView.buffer, this.dataView.byteOffset + this.offset, srcArray.byteLength);
      dstArray.set(srcArray);
      this.offset += size;
    }
    else {
      this.offset += size;
    }
  }
  
  setBinary(value) {
    const binaryBytes = value.byteLength;
    this.setDynamicBytes(binaryBytes);
    this.setRawBinary(value, binaryBytes);
  }
  
  setUint8Array(array) {
    this.setUint16(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setUint8(array[i]);
    }
  }
      
  setGuid(value) {
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(0)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(1)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(2)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(3)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(4)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(5)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(6)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(7)], true);
    
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(9)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(10)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(11)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(12)], true);
    
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(14)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(15)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(16)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(17)], true);
        
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(19)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(20)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(21)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(22)], true);
    
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(24)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(25)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(26)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(27)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(28)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(29)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(30)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(31)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(32)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(33)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(34)], false);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(35)], true);
  }
  
  setGuidArray(array) {
    this.setDynamicBytes(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setGuid(array[i]);
    }
  }
}


module.exports = Encoder;
