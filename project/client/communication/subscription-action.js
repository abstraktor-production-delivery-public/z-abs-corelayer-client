
'use strict';


class SubscriptionAction {
  constructor(data) {
    this.data = data;
  }
  
  getResponse() {
    return {
      data: this.data.data,
      isSuccess: () => {
        return 'success' === this.data.result.code;
      }
    }
  }
}


module.exports = SubscriptionAction;
