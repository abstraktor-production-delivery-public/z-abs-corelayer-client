
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class MessagePersistentInitRequest {
  constructor(name, subscribeServices) {
    this.msgId = CoreProtocolConst.PERSISTENT_INIT_REQUEST;
    this.name = name;
    this.subscribeServices = subscribeServices;
  }
}


module.exports = MessagePersistentInitRequest;
