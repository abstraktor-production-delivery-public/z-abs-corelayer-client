
'use strict';


class MessageRealtimeError {
  constructor() {
    this.onRealtimeName = 'onRealtimeMessageRealtimeError';
  }
}


module.exports = MessageRealtimeError;
