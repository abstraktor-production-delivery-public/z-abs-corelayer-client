
'use strict';

import WorkerMain from '../worker/worker-main';
const DataFetcherThread = requireAsBundle('./data-fetcher-thread');
import DataAction from './data-action';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';


class DataFetcher {
  constructor() {
    this.requests = new Map();
    this.workerMain = new WorkerMain(this, 'DataFetcherThread', DataFetcherThread);
    this.workerMain.start();
    this.init = false;
    this.initQueue = [];
  }
  
  send(dataAction, store, cb) {
    const guid = GuidGenerator.create();
    dataAction.setIds(guid);
    this.requests.set(guid, {
      dataAction: dataAction,
      store: store,
      cb: cb
    });
    if(this.init) {
      this.workerMain.postMessage(location.origin, dataAction.actionRequest);
    }
    else {
      this.initQueue.push([location.origin, dataAction.actionRequest]);
    }
  }
  
  onStarted(...params) {
    this.init = true;
    this.initQueue.forEach((params) => {
      this.workerMain.postMessage(...params);
    });
    this.initQueue = [];
  }
  
  onMessage(guid, responses) {
    if(null !== responses) {
      const cbData = this.requests.get(guid);
      if(cbData) {
        cbData.dataAction.setResponses(responses);
        cbData.store.dispatchDataAction(cbData.dataAction, cbData.cb);
      }
    }
    this.requests.delete(guid);
  }
}

DataFetcher.self = new DataFetcher();


module.exports = DataFetcher.self;
