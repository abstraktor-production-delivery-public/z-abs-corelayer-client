
'use strict';

import ActionRequest from 'z-abs-corelayer-cs/clientServer/communication/action-request';


export default class RealtimeAction {
  constructor() {
    this.actionRequest = new ActionRequest();
    this.actions = new Map();
  }

  addRequest(action, ...params) {
    this.actions.set(`${action._name}_0`, action);
    this.actionRequest.add(action._name, 0, ...params);
  }
  
  addRequestAction(action) {
    this.actions.set(`${action._name}_0`, action);
    this.actionRequest.add(action._name, 0, ...action.params);
  }
  
  addRequestIndex(action, index, ...params) {
    this.actions.set(`${action._name}_${index}`, action);
    this.actionRequest.add(action._name, index, ...params);
  }
  
  setIds(requestId, sessionId) {
    this.actionRequest.setIds(requestId, sessionId);
  }
  
  setResponses(responses) {
    if(null !== responses) {
      responses.forEach((response) => {
        const action = this.actions.get(`${response.name}_${response.index}`);
        if(action) {
          action.setResponse(response);
        }
      });
    }
  }
}
