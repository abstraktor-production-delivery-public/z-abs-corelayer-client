
'use strict';

import ActionData from 'z-abs-corelayer-cs/clientServer/communication/response-data';


export default class Action {
  constructor(...params) {
    this._name = this.constructor.name.substr(6);
    this.params = [];
    const fn = this.constructor.toString();
    const parameterNames = fn.slice(fn.indexOf('(') + 1, fn.indexOf(')')).match(/([^\s,]+)/g);
    params.forEach((param, index) => {
      if(!parameterNames[index].startsWith('_')) {
        this.params.push(param);
      }
      Reflect.set(this, parameterNames[index], param);
    });
    this.response = null;
    this._historyObject = {
      history: (href, options, state) => {
        ddb.warning(`${this.constructor.name} is not connected to history.`);
      }
    };
    this.onDataResponse = null;
  }
    
  setResponse(response) {
    this.response = new ActionData(response.result, response.data);
  }
  
  getResponse() {
    if(this.response) {
      return this.response;
    }
    else {
      return {
        isSuccess: () => {
          return false;
        }
      }
    }
  }
  
  setHistoryObject(historyObject) {
    if(this !== historyObject) {
      this._historyObject = historyObject;
    }
    return this;
  }
  
  history(href, options, state) {
    this._historyObject.history(href, options, state);
  }
  
  addOnDataResponse(onDataResponse) {
    this.onDataResponse = onDataResponse;
  }
}
