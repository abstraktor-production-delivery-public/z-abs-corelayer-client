
'use strict';


class DataConfig {
  constructor() {
    this.wsServers = new Map();
    this.loggedIn = false;
    this.loginCbs = [];
  }
  
  setWs(wsServers) {
    if(wsServers) {
      wsServers.forEach((wsServer) => {
        this.wsServers.set(wsServer.name, wsServer);
      });
    }
    this.loggedIn = true;
    this.loginCbs.forEach((cb) => {
      cb();
    });
    this.loginCbs = [];
  }
  
  login(cb) {
    if(this.loggedIn) {
      cb();
    }
    else {
      this.loginCbs.push(cb);
    }
  }
}


module.exports = new DataConfig();
