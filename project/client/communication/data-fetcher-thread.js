
'use strict';

const WorkerThread = require('../worker/worker-thread');


class DataFetcherThread extends WorkerThread{
  constructor(id) {
    super(id);
    this.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=utf-8'
    };
  }
  
  onMessage(origin, actionRequest) {
    const requestNames = actionRequest.requests.map((request) => {
      return request.name;
    });
    const requestPath = requestNames.join('/');
    fetch(origin + '/abs-data/' + requestPath, {
      method: 'post',
      headers: this.headers,
      body: JSON.stringify(actionRequest, (key, value) => {
        if(key.startsWith('_') && '_commentOut_' !== key) {
          return undefined;
        }
        return value;
      })
    }).then((response) => {
      return response.json();
    }).then((data) => {
      if(data) {
        this.postMessage(actionRequest.id, data.responses);
      }
      else {
        this.postMessage(actionRequest.id, null);
      }
    }).catch((err) => {
      this.postMessage(actionRequest.id, null);
    });
  }
}


module.exports = DataFetcherThread;
